####################################################
## Implementation of Different Large-Scale Models ##
###################################################
import numpy as np
from numba import njit, prange, cuda, f8


######################################################
##### Wilson-Cowan Model + Homeostatic Plasticity ####
######################################################

# Runs simulation of WC model
@njit(nogil=True)
def simulate_WC(init_CEI, t_len, W_mat, sim_time, store_time, dt, dt_save, rho, homeo_rate, speed, plast):
        
    ### Defines attributes for simulation
    W = W_mat.copy() # connectivity matrix

    # Number of neuronal populations
    n = t_len.shape[0]

    # Time constants for both populations
    tau_E = 2.5 # (ms) Excitatory time constant (tuned to gamma oscillations)
    tau_I = 5 # (ms) Inhibitory time constant

    # Constants for the response function F
    mu = 1 # firing response threshold
    sigma = 0.25 # firing response variability
    max_S = 1 # maximum response value

    # Standard deviation of additive noise      
    sd_noise = 0.01

    # Couplings from excitatory to inhibitory population
    C_EE = 3.5 # E->E coupling
    C_IE = 3.75 # E->I coupling
    C_EI = 2.5 * np.ones((n, 1)) # I->E coupling

    # Connectivity matrix and delays
    if speed > 0:
        delays_raw = t_len / speed
        delays = np.floor(delays_raw / dt)
        max_delay = int(np.amax(delays))
        delays = delays.astype(np.int64)

    # If speed is negative, interprets it as mean delay = 0
    else:
        delays = np.zeros((t_len.shape))
        max_delay = 0
        delays = delays.astype(np.int64)

    # External input to the excitatory population
    P = 0.31 # Defined to put the node at the transition between stable and oscillatory activity

    # Defines vectors to store activity. Storage starts in the points defined in store and store_time timesteps of activity are recorded
    # This is done to avoid huge arrays with mostly irrelevant data.
    store_time = min(store_time, sim_time)
    output = np.zeros((n, int(np.floor(store_time / dt_save))))
    store_count = 0 # counts timesteps when writing data to save

    # Fills past activity vector with random values around the target firing rate
    # This is a vector that keeps the state from the last max_delay timesteps.
    state = rho + sd_noise * np.random.rand(2, n, max_delay+1)

    # Initiates matrix to keep track of cIE as it evolves. Only stores every 10 seconds, for simplicity
    CEI_int = 10000 / dt # stores every 10 seconds (10000 ms)
    CEI_vec = np.zeros((n, int(np.floor(sim_time / (CEI_int*dt)))))
    if len(init_CEI) > 0:
        C_EI = init_CEI.copy()
    
    # Simulation
    for st in range(int(np.floor(sim_time / dt))): 

        # Gets current states (E, I) for all nodes
        curr_E = np.reshape(state[0, :, -1].copy(), (n, 1))
        curr_I = np.reshape(state[1, :, -1].copy(), (n, 1))

        # Computes delayed input from other nodes        
        state_E = state[0].copy()
        del_state = np.zeros((n, n))

        for i in range(n):
            for j in range(n):
                if W[i, j]>0:
                    del_state[i, j] = state_E[j, -(delays[i, j]+1)]

        # Multiplies connectivity matrix with the delayed state
        input_delayed = np.reshape(np.sum(np.multiply(W, del_state), axis = 1), (n, 1))        
               
        # Calculates state changes               
        E_input = C_EE * curr_E - np.multiply(C_EI, curr_I) + P + input_delayed + sd_noise * np.random.randn(n, 1)
        I_input = C_IE * curr_E + sd_noise * np.random.randn(n, 1)
        
        dE = (1/tau_E) * (-curr_E + F(E_input, max_S, mu, sigma))
        dI = (1/tau_I) * (-curr_I + F(I_input, max_S, mu, sigma))
        
        # Updates firing rate arrays using Euler method    
        new_E = curr_E + dt * dE
        new_I = curr_I + dt * dI
        state[0, :, :-1] = state[0, :, 1:]
        state[0, :, -1] = np.reshape(new_E, (n))
        state[1, :, :-1] = state[1, :, 1:]
        state[1, :, -1] = np.reshape(new_I, (n))

        # Stores data in specific time instants
        if st >= ((sim_time - store_time)/dt):
            if int((st % (dt_save/dt))) == 0:
                output[:, store_count] = np.reshape(new_E, (n))
                store_count += 1

        # Controls plasticity. If plast == 0, homeostatic plasticity is not implemented
        if plast == 1:
            C_EI = update_CEI(C_EI, homeo_rate, rho, curr_E, curr_I, dt)
            if (st % CEI_int) == 0:
                CEI_vec[:, int(st // CEI_int)] = np.reshape(C_EI.copy(), (n)) # stores cEI weights
    
    return output, CEI_vec



# Runs WC with an embedded algorithm to detect when local cIE weights reach a steadt state, stopping the simulation then
@njit(nogil=True)
def simulate_WC_steady_state(t_len, W_mat, sim_time, store_time, dt, dt_save, rho, homeo_rate, speed):

    ## Defines attributes for simulation    
    W = W_mat.copy() # connectivity matrix
    
    # Number of neuronal populations
    n = t_len.shape[0]

    # Time constants for both populations
    tau_E = 2.5 # (ms) Excitatory time constant (tuned to gamma oscillations)
    tau_I = 5 # (ms) Inhibitory time constant

    # Constants for the response function F
    mu = 1 # firing response threshold
    sigma = 0.25 # firing response variability
    max_S = 1 # maximum response value

    # Standard deviation of additive noise      
    sd_noise = 0.01

    # Couplings from excitatory to inhibitory population
    C_EE = 3.5 # E->E coupling
    C_IE = 3.75 # E->I coupling
    C_EI = 2.5 * np.ones((n, 1)) # I->E coupling

    # Connectivity matrix and delays
    if speed > 0:
        delays_raw = t_len / speed
        delays = np.floor(delays_raw / dt)
        max_delay = int(np.amax(delays))
        delays = delays.astype(np.int64)
    # If speed is negative, interprets it as mean delay = 0
    else:
        delays = np.zeros((t_len.shape))
        max_delay = 0
        delays = delays.astype(np.int64)
    
    # Intrinsic xcitability of the excitatory population
    P = 0.31 # Defined to control where nodes are relative to the transition between stable and oscillatory activity

    # Defines vectors to store activity. Storage starts in the points defined in store and store_time timesteps of activity are recorded
    # This is done to avoid huge arrays with mostly irrelevant data.
    store_time = min(store_time, sim_time)
    output = np.zeros((n, int(np.floor(store_time / dt_save))))
    store_count = 0 # counts timesteps when writing data to save

    # Fills past activity vector with random values around the target firing rate
    # This is a vector that keeps the state from the last max_delay timesteps.
    state = rho + sd_noise * np.random.rand(2, n, max_delay+1) # Fills data with random values around rho

    # Initiates matrix to keep track of cIE as it evolves. Only stores every 10 seconds, for simplicity
    CEI_int = 10000 / dt # stores every 10 seconds (10000 ms)
    CEI_vec = np.zeros((n, int(np.floor(sim_time / (CEI_int*dt)))))

    trigger_vec = np.zeros(W_mat.shape[0]) #binary vector, stores if node cEI is stable or not

    stop_window = 60 # Window to check variation of cEI. Window time is stop_window * CEI_int. e.g. stop_window = 60 || CEI_int = 10000 / dt --> 10 minutes
    
    # Simulation 
    stop = False # if True, simulation stops
    recording = False # if we are recording the signal or not
    st = 0 # timestep

    while ~stop and st <= (int(np.floor((sim_time + store_time) / dt)) + 1):

        # Gets current states (E, I) for all nodes
        curr_E = np.reshape(state[0, :, -1].copy(), (n, 1))
        curr_I = np.reshape(state[1, :, -1].copy(), (n, 1))

        # Computes delayed input from other nodes        
        state_E = state[0].copy()
        del_state = np.zeros((n, n))
        for i in range(n):
            for j in range(n):
                if W[i, j]>0:
                    del_state[i, j] = state_E[j, -(delays[i, j]+1)]

        # Multiplies connectivity matrix with the delayed state
        input_delayed = np.reshape(np.sum(np.multiply(W, del_state), axis = 1), (n, 1))        
               
        # Calculates state changes               
        E_input = C_EE * curr_E - np.multiply(C_EI, curr_I) + P + input_delayed + sd_noise * np.random.randn(n, 1)
        I_input = C_IE * curr_E + sd_noise * np.random.randn(n, 1)
        
        dE = (1/tau_E) * (-curr_E + F(E_input, max_S, mu, sigma))
        dI = (1/tau_I) * (-curr_I + F(I_input, max_S, mu, sigma))
        
        # Updates firing rate arrays using Euler method    
        new_E = curr_E + dt * dE
        new_I = curr_I + dt * dI
        state[0, :, :-1] = state[0, :, 1:]
        state[0, :, -1] = np.reshape(new_E, (n))
        state[1, :, :-1] = state[1, :, 1:]
        state[1, :, -1] = np.reshape(new_I, (n))

        # If time reaches the maximum amount stipulated by sim_time, starts recording
        if st >= int(np.floor(sim_time / dt)) and ~recording:
            recording = True

        # Controls plasticity. If we are recording, plasticity is not applied to avoid interactions
        if ~recording:

            C_EI = update_CEI(C_EI, homeo_rate, rho, curr_E, curr_I, dt) # updates local inhibitory weights

            if (st % CEI_int) == 0:

                CEI_vec[:, int(st // CEI_int)] = np.reshape(C_EI.copy(), (n)) # stores current weights

                ## Tests stop condition. Taking dCEI in the last stop_window, if its abs(mean) is smaller than its SEM for a given node, that node is considered stabilized
                if (st // CEI_int) > stop_window:

                    CEI_diff = numbadiff(CEI_vec[:, (int(st // CEI_int)-stop_window) : int(st // CEI_int)])

                    mean_vec = np.abs(numbamean(CEI_diff))
                    std_vec = numbastd(CEI_diff)

                    # Checks where abs(mean) is larger than SEM
                    th_vec = (1-(mean_vec > ((1/np.sqrt(stop_window)) * std_vec)))

                    # If node reaches steady-state for the first time, it adds 1 to its position in the trigger vec
                    trigger_vec += th_vec
                    trigger_vec[trigger_vec > 1] = 1

                    ## If all nodes are stable, stops plasticity and starts recording
                    if np.all(trigger_vec):

                        CEI_vec = CEI_vec[:, :int(st // CEI_int)]

                        recording = True

        # If recording started, stores data from excitatory population
        if recording:
            if store_count < int(np.floor(store_time / dt_save)):
                if int((st % (dt_save/dt))) == 0:
                    output[:, store_count] = np.reshape(new_E, (n))
                    store_count += 1

            # If store_time has been reached, stops simulation
            elif store_count >= int(np.floor(store_time / dt_save)):
                stop = True


        # Increments steps
        st += 1
    
    return output, CEI_vec

#########################
## Stuart-Landau Model ##
#########################

@njit(nogil=True)
def simulate_SL(t_len, W_mat, sim_time, store_time, dt, dt_save, a_bif, speed):
    
    W = W_mat.copy()
    
    ### Defines attributes for simulation
    # Number of neuronal populations
    n = t_len.shape[0]

    # Parameters of Stuart-Landau Model
    w = 2*np.pi*40 # frequency of oscillation
    a = a_bif # bifurcation parameter
    b = 0.001 # noise

    # Connectivity matrix and delays
    if speed > 0:
        delays_raw = t_len / speed
        delays = np.floor(delays_raw / dt)
        max_delay = int(np.amax(delays))
        delays = delays.astype(np.int64)
    
    # If speed is negative, interprets it as mean delay = 0
    else:
        delays = np.zeros((t_len.shape))
        max_delay = 0
        delays = delays.astype(np.int64)
    
    # Defines vectors to store activity. Storage starts in the points defined in store and store_time timesteps of activity are recorded
    # This is done to avoid huge arrays with mostly irrelevant data.
    store_time = min(store_time, sim_time)
    output = np.zeros((n, int(np.floor(store_time / dt_save))), dtype = np.complex_)
    store_count = 0

    # Fills past activity vector with random values around the target firing rate
    # This is a vector that keeps the state from the last max_delay timesteps.
    state = b*np.random.rand(n, max_delay+1) + b*1J*np.random.rand(n, max_delay+1)

    # Runs simulations
    for st in range(int(np.floor(sim_time / dt))): 

        # Gets current state (z) for all nodes
        curr_z = np.reshape(state[:, -1].copy(), (n, 1))

        # Computes delayed input from other nodes        
        state_z = state.copy()
        del_state = np.zeros((n, n), dtype = np.complex_)

        for i in range(n):
            for j in range(n):
                if W[i, j]>0:
                    del_state[i, j] = (state_z[j, -(delays[i, j]+1)] - state_z[i, -1])

        # Multiplies connectivity matrix with the delayed state
        input_delayed = np.reshape(np.sum(np.multiply(W, del_state), axis = 1), (n, 1))

        # Calculates state changes  
        dz = curr_z * (a + (w * 1J) - np.abs(curr_z**2)) + \
             input_delayed + b*(np.random.randn(n, 1) + 1J*np.random.randn(n, 1))
        
        # Updates firing rate and state variable arrays using Euler method            
        new_z = curr_z + (dt*1e-3) * dz # dt has to be in seconds
        state[:, :-1] = state[:, 1:]
        state[:, -1] = np.reshape(new_z, (n))


        # Stores data
        if st >= ((sim_time - store_time)/dt):
            if int((st % (dt_save/dt))) == 0:
                output[:, store_count] = np.reshape(new_z, (n))
                store_count += 1
    
    return output



#############################
## Ballon-Windkessel Model ##
#############################
# Forward model to extract BOLD equivalent signals from firing-rate equivalent signals

@njit(nogil=True)
def balloon_windkessel(signals, dt):
    
    BOLD = np.zeros(np.shape(signals))
    
    k = 0.65 # [1/s] rate of signal decay 
    gamma = 0.41  # [1/s] rate of flow-dependent elimination
    tau = 0.98 # [s] hemodynamic transit time
    alpha = 0.32 # Grubbs exponent
    rho = 0.34 # resting oxygen extraction fraction
    V0 = 0.02 # resting blood volume fraction
    dt = dt / 1000 # [s]
    
    n = np.shape(signals)[0]
    
    curr_s = 1e-10 * np.ones((1, n))
    curr_f = 1e-10 * np.ones((1, n))
    curr_v = 1e-10 * np.ones((1, n))
    curr_q = 1e-10 * np.ones((1, n))
    
    for i in range(1, np.shape(signals)[1]):

        new_q = curr_q + dt * (1/tau) * (curr_f/rho * (1-np.power((1-rho), (1/curr_f))) - np.power(curr_v, (1/alpha) - 1) * curr_q)

        new_v = curr_v + dt * (1/tau) * (curr_f - np.power(curr_v, 1/alpha))

        new_f = curr_f + dt * curr_s
    
        new_s = curr_s + dt * (np.reshape(signals[:, i-1].copy(), (1, n)) - k * curr_s - gamma*(curr_f - 1))

        y = V0 * (7*rho*(1-curr_q) + 2*(1-np.divide(curr_q, curr_v)) + (2*rho-0.2)*(1 - curr_v))

        curr_q = new_q
        curr_v = new_v
        curr_f = new_f
        curr_s = new_s

        BOLD[:, i] = np.reshape(y, (n))

    return BOLD




######################
## Helper functions ##
######################
# These are necessary to run the code properly with jitted functions


@njit(nogil=True)
def numbadiff(x):
    return (x[:, 1:] - x[:, :-1])

@njit(nogil=True)
def numbamean(x):
    res = []
    for i in range(x.shape[0]):
        res.append(x[i, :].mean())
    return np.array(res)

@njit(nogil=True)
def numbastd(x):
    res = []
    for i in range(x.shape[0]):
        res.append(x[i, :].std())
    return np.array(res)


# Activation function of WC populations
@njit(nogil=True)
def F(x, max_S, mu, sigma):
    return max_S / (1 + np.exp((-(x - mu)/sigma)))


# Homeostatic plasticity - updates local inhibitory weights
@njit(nogil=True)
def update_CEI(C_EI, homeo_rate, rho, E, I, dt):
    # Updates cEI weight according to activity of excitatory population
    dC_EI = homeo_rate * np.multiply(I, E - rho)
    new_CEI = C_EI + dt * dC_EI
    
    return new_CEI