import numpy as np
from numba import njit
import WBM as WBM
import scipy.signal as sig
import time as time
from pymatreader import read_mat


def run_WC_model(K, rho, md):

  '''
  Runs simulation of the WC model for the combination of free parameters specified as arguments
  ands stores MEG signals, BOLD signals, or both.

  Args:
    K (float): global coupling, scaling factor for structural connectivity
    rho (float): target firing rate for homeostatic plasticity. Should be between 0 and 1, but we recommend 0 to 0.3 for sensible results.
    md (float): mean conduction delay in ms. This is used with the loaded connection lengths to compute conduction speed
  '''

  # Loading connectivity information
  mat = read_mat('Data/Connectivity/SC_90aal_32HCP.mat')

  # Structural connectivity
  connect = mat['mat']
  connect[connect < 10] = 0 # Removes negligible connections with less than 10 fibers
  np.fill_diagonal(connect, 0) # Ensures there are no self connections
  connect /= np.max(connect) # Normalizes connectivity matrix so that maximum value is 1

  # Tract lengths
  t_len = mat['mat_D']  

  # Picks either only cortical areas or all areas. This only works for the AAL90 atlas!!!
  cortical = True
  if cortical:
      mask = np.concatenate((np.arange(36),
                             np.array([38, 39]),
                             np.arange(42, 70),
                             np.arange(78, 90)))
              
      connect = connect[:, mask][mask]
      t_len = t_len[:, mask][mask]

  # Computes conduction velocity from md parameter and tract lengths
  if md == 0:
      speed = -10 # If speed is negative, WBM functions recognize this as md = 0. This was done because conduction velocity cannot be infinite
  else:
      speed = np.mean(t_len[connect>0])/md # [connect > 0] ensures that we only account for connectioms between nodes that are actually connected


  #################
  ## Compilation ##
  #################

  # This step just runs the functions once with a short simulation time so that numba compiles them before running the actual simulation.
  # This should only take a few seconds
  w_test = connect.copy()
  test_sig, _ = WBM.simulate_WC_steady_state(t_len = t_len, 
                                             W_mat = w_test, 
                                             sim_time = 1000, 
                                             store_time = 1000, 
                                             dt = 1, 
                                             dt_save = 1, 
                                             rho = 0.1, 
                                             homeo_rate = (1/2.5e3), 
                                             speed = speed)

  test = WBM.balloon_windkessel(test_sig, dt = 1)
  print('Compilation done!')


  ################
  ## Simulation ##
  ################

  # Simulation parameters
  sim_time = 36*60*1000 # Maximum simulation time, in ms
  store_time = 35*60*1000 # Total time to be recorded, in ms
  dt = 0.2 # ms. Integration time step of simulations
  dt_save = 1 # ms. Time step to store model results, we chose 1ms to improve tractability

  # Homeostatic plasticity learning rate.
  homeo_rate = 1/2.5e3 # (1/ms)

  # Scales structural connectivity
  W = K*connect.copy()

  # Runs simulation
  print('#####################################')
  print('############Simulating...############')
  print('#####################################')
  print('K = {} || rho = {} || md = {}'.format(np.around(K, 2), 
                                               np.around(rho, 2), 
                                               md))

  start = time.time()
  output, CEI_vec = WBM.simulate_WC_steady_state(t_len = t_len,
                                                 W_mat = W, 
                                                 sim_time = sim_time, 
                                                 store_time = store_time, 
                                                 dt = dt, 
                                                 dt_save = dt_save, 
                                                 rho = rho, 
                                                 homeo_rate = homeo_rate, 
                                                 speed = speed)


  ## Define modalities to store
  modality = 'all' # Options: MRI, MEG, MRI+MEG, MRI+MEG_ds, all
  t_MEG = 360 * 1000 # Length of MEG segment to be stored, in ms

  save_CEI = True # if True, saves cEI in the BOLD_results folder
  if save_CEI:
      np.save(r"Data/WC_results/BOLD/cEI_K{}_rho{}_md{}.npy".format(np.around(K, 3), 
                                                                    np.around(rho, 3),
                                                                    md), save_CEI)  	

  signal_raw = output.copy()

  if modality == 'MEG':

      frs = 250
      fs = 1e3/dt_save
      signal = signal_raw[:, -int(t_MEG * dt_save):]
      n_samples = int(frs * (signal.shape[-1]) / fs)
      signal_MEG = np.zeros((signal.shape[0], n_samples))
      for n in range(signal.shape[0]):
          signal_MEG[n, :] = sig.resample(signal[n, :], n_samples)

      np.save(r"Data/WC_results/MEG/MEG_K{}_rho{}_md{}.npy".format(np.around(K, 3), 
                                                                   np.around(rho, 3),
                                                                   md), signal_MEG)

  # Saves amplitude envelopes of theta, alpha and beta bands, lowpass filtered and downsampled. 
  # The filenames used to store downsampled signals will end in "band_ds.npy", e.g. "MEG_K5_rho0.22_md4_theta_ds.npy" 
  elif modality == 'MEG_ds':
      signal = signal_raw[:, -int(t_MEG * dt_save):]
      frs = 250
      fs = 1e3/dt_save
      n_samples = int(frs * (signal.shape[-1])/fs)
      signal_MEG = np.zeros((signal.shape[0], n_samples))
      for n in range(signal.shape[0]):
          signal_MEG[n, :] = sig.resample(signal[n, :], n_samples)

      # Prepares filter
      bt, at = sig.butter(2, [4, 8], btype = 'band', output = 'ba', fs = frs)
      ba, aa = sig.butter(2, [8, 13], btype = 'band', output = 'ba', fs = frs)  
      bb, ab = sig.butter(2, [13, 30], btype = 'band', output = 'ba', fs = frs)

      # Filters signals
      signal_t = sig.filtfilt(bt, at, signal_MEG, axis = -1)
      signal_a = sig.filtfilt(ba, aa, signal_MEG, axis = -1)
      signal_b = sig.filtfilt(bb, ab, signal_MEG, axis = -1)

      # Computes hilber amplitude envelopes
      signal_t_h = np.abs(sig.hilbert(signal_t, axis = -1))
      signal_a_h = np.abs(sig.hilbert(signal_a, axis = -1))
      signal_b_h = np.abs(sig.hilbert(signal_b, axis = -1))

      # Filters envelopes
      b, a = sig.butter(2, 0.5, btype = 'lowpass', output = 'ba', fs = frs)
      signal_t_h_ds = sig.filtfilt(b, a, signal_t_h, axis = -1)
      signal_a_h_ds = sig.filtfilt(b, a, signal_a_h, axis = -1)
      signal_b_h_ds = sig.filtfilt(b, a, signal_b_h, axis = -1)

      # Downsamples envelopes
      fs_new = 5
      signal_t_h_ds = signal_t_h_ds[:, ::int(frs/fs_new)]
      signal_a_h_ds = signal_a_h_ds[:, ::int(frs/fs_new)]
      signal_b_h_ds = signal_b_h_ds[:, ::int(frs/fs_new)]

      # Saves downsampled signals
      np.save(r"Data/WC_results/MEG/MEG_K{}_rho{}_md{}_theta_ds.npy".format(np.around(K, 2), 
                                                                            np.around(rho, 2),
                                                                            md), signal_t_h_ds)      

      np.save(r"Data/WC_results/MEG/MEG_K{}_rho{}_md{}_alpha_ds.npy".format(np.around(K, 2), 
                                                                            np.around(rho, 2),
                                                                            md), signal_a_h_ds)

      np.save(r"Data/WC_results/MEG/MEG_K{}_rho{}_md{}_beta_ds.npy".format(np.around(K, 2), 
                                                                           np.around(rho, 2),
                                                                           md), signal_b_h_ds)

  elif modality == 'MRI':

      signal = WBM.balloon_windkessel(signal_raw, dt_save)

      frs = 1/(0.72)
      fs = 1e3/dt_save
      nrs = int(fs/frs)
      bdr = int(2.5 * 60 * fs) # Time to remove at the beginning and end of signal, to avoid boundary effects from the Balloon-Windkessel model

      n_samples = int(frs * (signal.shape[-1] - 2 * bdr) / fs)
      signal_MRI = np.zeros((signal.shape[0], n_samples))

      for n in range(signal.shape[0]):
          signal_MRI[n, :] = sig.resample(signal[n, bdr:-bdr], n_samples)

      np.save(r"Data/WC_results/BOLD/BOLD_K{}_rho{}_md{}.npy".format(np.around(K, 3), 
                                                                     np.around(rho, 3),
                                                                     md), signal_MRI)

  elif modality == 'MRI+MEG':

      # BOLD Signal
      signal = WBM.balloon_windkessel(signal_raw, dt_save)

      frs = 1/(0.72)
      fs = 1e3/dt_save
      nrs = int(fs/frs)
      bdr = int(2.5 * 60 * fs) # Time to remove at the beginning and end of signal, to avoid boundary effects from the Balloon-Windkessel model

      n_samples = int(frs * (signal.shape[-1] - 2 * bdr) / fs)
      signal_MRI = np.zeros((signal.shape[0], n_samples))

      for n in range(signal.shape[0]):
          signal_MRI[n, :] = sig.resample(signal[n, bdr:-bdr], n_samples)

      np.save(r"Data/WC_results/BOLD/BOLD_K{}_rho{}_md{}.npy".format(np.around(K, 3), 
                                                                     np.around(rho, 3),
                                                                     md), signal_MRI)

      # MEG Signal
      frs = 250
      fs = 1e3/dt_save
      signal = signal_raw[:, -(bdr + int(t_MEG * dt_save)):-bdr] # MEG signals will be equivalent to the last t_MEG time of the MRI signals
      n_samples = int(frs * (signal.shape[-1]) / fs)
      signal_MEG = np.zeros((signal.shape[0], n_samples))

      for n in range(signal.shape[0]):
          signal_MEG[n, :] = sig.resample(signal[n, :], n_samples)

      np.save(r"Data/WC_results/MEG/MEG_K{}_rho{}_md{}.npy".format(np.around(K, 3), 
                                                                   np.around(rho, 3),
                                                                   md), signal_MEG)

  elif modality == 'MRI+MEG_ds':

      # BOLD Signal
      signal = WBM.balloon_windkessel(signal_raw, dt_save)

      frs = 1/(0.72)
      fs = 1e3/dt_save
      nrs = int(fs/frs)
      bdr = int(2.5 * 60 * fs) # Time to remove at the beginning and end of signal, to avoid boundary effects from the Balloon-Windkessel model

      n_samples = int(frs * (signal.shape[-1] - 2 * bdr) / fs)
      signal_MRI = np.zeros((signal.shape[0], n_samples))

      for n in range(signal.shape[0]):
          signal_MRI[n, :] = sig.resample(signal[n, bdr:-bdr], n_samples)

      np.save(r"Data/WC_results/BOLD/BOLD_K{}_rho{}_md{}.npy".format(np.around(K, 3), 
                                                                     np.around(rho, 3),
                                                                     md), signal_MRI)


      # Saves amplitude envelopes of theta, alpha and beta bands, lowpass filtered and downsampled. 
      # The filenames used to store downsampled signals will end in "band_ds.npy", e.g. "MEG_K5_rho0.22_md4_theta_ds.npy" 
      signal = signal_raw[:, -int(t_MEG * dt_save):]
      ns = signal.shape[-1]
      frs = 250
      fs = 1e3/dt_save
      n_samples = int(frs * (signal.shape[-1])/fs)
      signal_MEG = np.zeros((signal.shape[0], n_samples))
      for n in range(signal.shape[0]):
          signal_MEG[n, :] = sig.resample(signal[n, :], n_samples)

      # Prepares filter
      bt, at = sig.butter(2, [4, 8], btype = 'band', output = 'ba', fs = frs)
      ba, aa = sig.butter(2, [8, 13], btype = 'band', output = 'ba', fs = frs)  
      bb, ab = sig.butter(2, [13, 30], btype = 'band', output = 'ba', fs = frs)

      # Filters signals
      signal_t = sig.filtfilt(bt, at, signal_MEG, axis = -1)
      signal_a = sig.filtfilt(ba, aa, signal_MEG, axis = -1)
      signal_b = sig.filtfilt(bb, ab, signal_MEG, axis = -1)

      # Computes hilber amplitude envelopes
      signal_t_h = np.abs(sig.hilbert(signal_t, axis = -1))
      signal_a_h = np.abs(sig.hilbert(signal_a, axis = -1))
      signal_b_h = np.abs(sig.hilbert(signal_b, axis = -1))

      # Filters envelopes
      b, a = sig.butter(2, 0.5, btype = 'lowpass', output = 'ba', fs = frs)
      signal_t_h_ds = sig.filtfilt(b, a, signal_t_h, axis = -1)
      signal_a_h_ds = sig.filtfilt(b, a, signal_a_h, axis = -1)
      signal_b_h_ds = sig.filtfilt(b, a, signal_b_h, axis = -1)

      # Downsamples envelopes
      fs_new = 5
      signal_t_h_ds = signal_t_h_ds[:, ::int(frs/fs_new)]
      signal_a_h_ds = signal_a_h_ds[:, ::int(frs/fs_new)]
      signal_b_h_ds = signal_b_h_ds[:, ::int(frs/fs_new)]

      # Saves downsampled signals
      np.save(r"Data/WC_results/MEG/MEG_K{}_rho{}_md{}_theta_ds.npy".format(np.around(K, 2), 
                                                                            np.around(rho, 2),
                                                                            md), signal_t_h_ds)      

      np.save(r"Data/WC_results/MEG/MEG_K{}_rho{}_md{}_alpha_ds.npy".format(np.around(K, 2), 
                                                                            np.around(rho, 2),
                                                                            md), signal_a_h_ds)

      np.save(r"Data/WC_results/MEG/MEG_K{}_rho{}_md{}_beta_ds.npy".format(np.around(K, 2), 
                                                                           np.around(rho, 2),
                                                                           md), signal_b_h_ds)


  elif modality == 'all':
      
      # BOLD Signal
      signal = WBM.balloon_windkessel(signal_raw, dt_save)

      frs = 1/(0.72)
      fs = 1e3/dt_save
      nrs = int(fs/frs)
      bdr = int(2.5 * 60 * fs) # Time to remove at the beginning and end of signal, to avoid boundary effects from the Balloon-Windkessel model

      n_samples = int(frs * (signal.shape[-1] - 2 * bdr) / fs)
      signal_MRI = np.zeros((signal.shape[0], n_samples))

      for n in range(signal.shape[0]):
          signal_MRI[n, :] = sig.resample(signal[n, bdr:-bdr], n_samples)

      np.save(r"Data/WC_results/BOLD/BOLD_K{}_rho{}_md{}.npy".format(np.around(K, 3), 
                                                                     np.around(rho, 3),
                                                                     md), signal_MRI)


      # Saves amplitude envelopes of theta, alpha and beta bands, lowpass filtered and downsampled. 
      # The filenames used to store downsampled signals will end in "band_ds.npy", e.g. "MEG_K5_rho0.22_md4_theta_ds.npy" 
      signal = signal_raw[:, -int(t_MEG * dt_save):]
      ns = signal.shape[-1]
      frs = 250
      fs = 1e3/dt_save
      n_samples = int(frs * (signal.shape[-1])/fs)
      signal_MEG = np.zeros((signal.shape[0], n_samples))
      for n in range(signal.shape[0]):
          signal_MEG[n, :] = sig.resample(signal[n, :], n_samples)

      # Prepares filter
      bt, at = sig.butter(2, [4, 8], btype = 'band', output = 'ba', fs = frs)
      ba, aa = sig.butter(2, [8, 13], btype = 'band', output = 'ba', fs = frs)  
      bb, ab = sig.butter(2, [13, 30], btype = 'band', output = 'ba', fs = frs)

      # Filters signals
      signal_t = sig.filtfilt(bt, at, signal_MEG, axis = -1)
      signal_a = sig.filtfilt(ba, aa, signal_MEG, axis = -1)
      signal_b = sig.filtfilt(bb, ab, signal_MEG, axis = -1)

      # Computes hilber amplitude envelopes
      signal_t_h = np.abs(sig.hilbert(signal_t, axis = -1))
      signal_a_h = np.abs(sig.hilbert(signal_a, axis = -1))
      signal_b_h = np.abs(sig.hilbert(signal_b, axis = -1))

      # Filters envelopes
      b, a = sig.butter(2, 0.5, btype = 'lowpass', output = 'ba', fs = frs)
      signal_t_h_ds = sig.filtfilt(b, a, signal_t_h, axis = -1)
      signal_a_h_ds = sig.filtfilt(b, a, signal_a_h, axis = -1)
      signal_b_h_ds = sig.filtfilt(b, a, signal_b_h, axis = -1)

      # Downsamples envelopes
      fs_new = 5
      signal_t_h_ds = signal_t_h_ds[:, ::int(frs/fs_new)]
      signal_a_h_ds = signal_a_h_ds[:, ::int(frs/fs_new)]
      signal_b_h_ds = signal_b_h_ds[:, ::int(frs/fs_new)]

      # Saves downsampled signals
      np.save(r"Data/WC_results/MEG/MEG_K{}_rho{}_md{}_theta_ds.npy".format(np.around(K, 2), 
                                                                            np.around(rho, 2),
                                                                            md), signal_t_h_ds)      

      np.save(r"Data/WC_results/MEG/MEG_K{}_rho{}_md{}_alpha_ds.npy".format(np.around(K, 2), 
                                                                            np.around(rho, 2),
                                                                            md), signal_a_h_ds)

      np.save(r"Data/WC_results/MEG/MEG_K{}_rho{}_md{}_beta_ds.npy".format(np.around(K, 2), 
                                                                           np.around(rho, 2),
                                                                           md), signal_b_h_ds)


      # Saves 50s of "raw" MEG signal
      t_MEG = 50 * 1000 # Length of MEG segment to be stored, in ms
      frs = 250
      fs = 1e3/dt_save
      signal = signal_raw[:, -int(t_MEG * dt_save):]
      n_samples = int(frs * (signal.shape[-1]) / fs)
      signal_MEG = np.zeros((signal.shape[0], n_samples))
      for n in range(signal.shape[0]):
          signal_MEG[n, :] = sig.resample(signal[n, :], n_samples)

      np.save(r"Data/WC_results/MEG/MEG_K{}_rho{}_md{}.npy".format(np.around(K, 3), 
                                                                   np.around(rho, 3),
                                                                   md), signal_MEG)     


  end = time.time()
  print('Elapsed time: {}'.format(np.around((end-start)/60, 2)))