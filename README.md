# Whole Brain Generative Models
This code goes along with the manuscript "Multi-modal and multi-model interrogation of large-scale functional brain networks".

Authors:  Francesca Castaldo,  Francisco Páscoa dos Santos, Ryan C Timms,  Joana Cabral,  Jakub Vohryzek, Gustavo Deco, Mark Woolrich,  Karl Friston, Paul Verschure, Vladimir Litvak

Correspondence: Francisco Páscoa dos Santos, f.pascoadossantos@gmail.com


## Introduction
Current whole-brain models are generally tailored to the modelling of a particular modality of data (e.g., fMRI or MEG/EEG). Although different imaging modalities reflect different aspects of neural activity, we hypothesise that this activity arises from common network dynamics. Building on the universal principles of self-organising delay-coupled nonlinear systems, we aim to link distinct electromagnetic and metabolic features of brain activity to the dynamics on the brain’s macroscopic structural connectome.

To jointly predict dynamical and functional connectivity features of distinct signal modalities, we consider two large-scale models generating local short-lived 40 Hz oscillations with various degrees of realism - namely Stuart Landau (SL) and Wilson and Cowan with excitatory-inhibitory homeostasis (balanced WC) models. To this end, we measure features of functional connectivity and metastable oscillatory modes (MOMs) in fMRI and MEG signals - and compare them against simulated data.

In this repo, we added example code to run simulations for both models with the possibility of varying 3 free parameters: global coupling (K), mean delay and target firing rate ($\rho$) or bifurcation parameter (a) for the balanced WC and SL, respectively. In addition, we include a few notebooks exemplifying the most relevant steps of the analysis of connectivity, dynamics and MOMs.

## Simulation

In the Simulation folder you will find 3 .py scripts:

- **WBM.py**: contains the core code for the models and necessary functions to run it. The functions are numba compiled to improve simulation runtime.

- **run_WC_model.py**: this script contains a function that you can execute to run one simulation of the balanced WC model. This script will run one simulation and store BOLD and MEG signals. This function takes three arguments: `K`, `rho` and `md` corresponding, respectively, to the global coupling, the target firing rate and mean delay. Due to the implementation of excitatory-inhibitory homeostasis, simulations need to run for some time while local inhibitory weights are modified until they reach a steady-state of balanced local activity. For this reason, this function has an embeded algorithm that tests if local inhibitory weights have reached a steady state. If this condition is reached or the maximum simulation time is reached (default is 500 min of simulated time) the signals are recorded. For more details on this algorithm, feel free to check the manuscript (https://doi.org/10.1101/2022.12.19.520967).
You can run this code from the terminal in the following way (arguments are just examples):

```
cd YOUR_FOLDER
python -c "import run_WC_model; run_WC_model.run_WC_model(4, 0.22, 4)"
```

- **run_SL_model.py**: this script contains a function that you can execute to run one simulation of the SL model. This script will run one simulation and store BOLD and MEG signals. This function takes three arguments: `K`, `a_bif` and `md` corresponding, respectively, to the global coupling, the bifurcation parameter and mean delay.
You can run this code from the terminal in the following way (arguments are just examples):

```
cd YOUR_FOLDER
python -c "import run_SL_model; run_SL_model.run_SL_model(250, -5, 4)"
```

## Analysis

In this folder, we included an Ipython notebook containing example analysis for the main features that we evaluted in the paper: functional connectivity, functional connectivity dynamics and MOMs. The notebook is commented so that you can be guided through the main steps necessary for the analysis. For now, we have left a few example simulations of the balanced WC model in the 'Simulation/Data/WC_results' folder, so that you can test the analysis code.

